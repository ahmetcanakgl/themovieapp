package co.aca.themovieapp.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.aca.themovieapp.di.ViewModelFactory
import co.aca.themovieapp.di.ViewModelKey
import co.aca.themovieapp.ui.detail.FRMovieDetailViewModel
import co.aca.themovieapp.ui.main.ACMovieListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AppViewModels {


    @Binds
    @IntoMap
    @ViewModelKey(ACMovieListViewModel::class)
    abstract fun bindACMovieListViewModel(repoViewModel: ACMovieListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FRMovieDetailViewModel::class)
    abstract fun bindFRMovieDetailViewModel(repoViewModel: FRMovieDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}