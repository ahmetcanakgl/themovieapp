package co.aca.themovieapp.base

import co.aca.themovieapp.di.ActivityScoped
import co.aca.themovieapp.di.FragmentScoped
import co.aca.themovieapp.ui.detail.FRMovieDetail
import co.aca.themovieapp.ui.main.ACMovieList
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = [AndroidInjectionModule::class])
abstract class AppPages {

    @ActivityScoped
    @ContributesAndroidInjector
    abstract fun bindACMovieList(): ACMovieList

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun bindFRMovieDetail(): FRMovieDetail

}