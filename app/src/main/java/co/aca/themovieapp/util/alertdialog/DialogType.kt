package co.aca.themovieapp.util.alertdialog

enum class DialogType {
    SUCCESS, WAITING, SAVING, UNSUCCESSFUL, INFO
}