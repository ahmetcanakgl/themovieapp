package co.aca.themovieapp.util

import androidx.fragment.app.Fragment
import co.aca.themovieapp.util.alertdialog.DialogContent
import co.aca.themovieapp.util.fragment.FragmentFactory
import kotlin.reflect.KClass

interface ViewListener {

    fun showDialog(dialogContent: DialogContent)

    fun showToast(text: String)

    fun showFragment(factory: FragmentFactory)

    fun showLoading()

    fun hideLoading()

    fun goBack()

    fun returnPage(clazz: KClass<out Fragment>)


}