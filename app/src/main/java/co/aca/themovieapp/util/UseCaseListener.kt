package co.aca.themovieapp.util

import co.aca.themovieapp.util.alertdialog.DialogContent

interface UseCaseListener {

    fun showDialog(dgContent: DialogContent)

    fun addToQueue(tag: String)

    fun removeFromQueue(tag: String)

    fun timeoutExpired(message: String)
}