package co.aca.themovieapp.util.alertdialog


interface DialogActionListener {
    fun onClicked()
}