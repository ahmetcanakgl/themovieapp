package co.aca.themovieapp.util.fragment

enum class TransitionType {
    ADD, REPLACE, SHOW, HIDE
}