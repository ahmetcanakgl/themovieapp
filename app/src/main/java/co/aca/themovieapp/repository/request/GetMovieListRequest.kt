package co.aca.themovieapp.repository.request

import co.aca.themovieapp.repository.base.BaseRequest

class GetMovieListRequest : BaseRequest() {

    var pageNumber: Int = 1
}