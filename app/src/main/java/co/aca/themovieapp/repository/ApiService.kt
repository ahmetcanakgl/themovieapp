package co.aca.themovieapp.repository

import co.aca.themovieapp.repository.response.MovieListResponse
import io.reactivex.Single
import retrofit2.http.*

interface ApiService {

    @GET("3/movie/top_rated")
    fun getMovieList(
        @Query("api_key") apiKey: String,
        @Query("language") language: String,
        @Query("page") page: Int
    ): Single<MovieListResponse>

}