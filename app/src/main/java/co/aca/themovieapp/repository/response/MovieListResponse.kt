package co.aca.themovieapp.repository.response

import co.aca.themovieapp.repository.response.model.MovieModel
import co.aca.themovieapp.repository.base.BaseResponse
import com.google.gson.annotations.SerializedName

open class MovieListResponse : BaseResponse() {

    @SerializedName("results")
    var results: ArrayList<MovieModel>? = null
}