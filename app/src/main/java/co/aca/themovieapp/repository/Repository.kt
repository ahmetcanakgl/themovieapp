package co.aca.themovieapp.repository

import co.aca.themovieapp.BuildConfig
import co.aca.themovieapp.repository.response.MovieListResponse
import io.reactivex.Single
import javax.inject.Inject

class Repository @Inject constructor(private var apiService: ApiService) {

    fun getMovieList(page: Int): Single<MovieListResponse> {
        return apiService.getMovieList(
            apiKey = BuildConfig.API_KEY,
            language = "en-us",
            page = page
        )
    }
}