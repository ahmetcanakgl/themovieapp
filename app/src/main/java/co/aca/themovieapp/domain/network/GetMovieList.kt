package co.aca.themovieapp.domain.network

import co.aca.themovieapp.domain.base.BaseUseCase
import co.aca.themovieapp.repository.Repository
import co.aca.themovieapp.repository.response.MovieListResponse
import javax.inject.Inject

class GetMovieList @Inject constructor(private val repository: Repository) :
    BaseUseCase<MovieListResponse, GetMovieList.Params>() {

    private var pageNumber = 1
    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    var isLoadingVisible: Boolean = false

    var response = MovieListResponse()

    class Params(val isLoadingVisible: Boolean)

    override fun execute(params: Params) {

        isLoading = true
        isLoadingVisible = params.isLoadingVisible

        sendRequest {
            repository.getMovieList(pageNumber)
        }
    }

    override fun onSendSuccess(t: MovieListResponse) {
        isLoading = false
        if (pageNumber == 1) {
            clearList()
        }

        response.results?.addAll(t.results!!)

        if (t.results!!.size < 20) {
            isLastPage = true
        }

        pageNumber++

        super.onSendSuccess(response)
    }

    fun clearPage() {
        response.results = null
        clearList()
    }

    fun clearList() {
        if (response.results != null) {
            response.results!!.clear()
        } else {
            response.results = ArrayList()
        }

        isLastPage = false
        isLoading = false
        pageNumber = 1
    }

    override fun isLoadingEnable(): Boolean {
        return isLoadingVisible
    }
}