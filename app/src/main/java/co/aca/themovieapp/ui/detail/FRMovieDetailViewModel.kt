package co.aca.themovieapp.ui.detail

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import co.aca.themovieapp.base.BaseViewModel
import co.aca.themovieapp.repository.response.model.MovieModel
import javax.inject.Inject

class FRMovieDetailViewModel @Inject constructor() : BaseViewModel() {

    var movieModel: MovieModel? = null

    lateinit var onMovieDetailReceived: MutableLiveData<MovieModel>

    override fun onCreate(savedInstanceState: Bundle?, arguments: Bundle?) {
        super.onCreate(savedInstanceState, arguments)

        onMovieDetailReceived = MutableLiveData()

        if (arguments != null) {
            movieModel = arguments!!.getSerializable("movieModel") as MovieModel?

            if (movieModel != null) {
                onMovieDetailReceived.postValue(movieModel)

            }
        }
    }
}