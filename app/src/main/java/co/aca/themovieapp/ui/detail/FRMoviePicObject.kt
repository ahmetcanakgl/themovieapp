package co.aca.themovieapp.ui.detail

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import co.aca.themovieapp.R
import co.aca.themovieapp.ui.detail.MoviePictureAdapter.Companion.ARG_OBJECT
import com.squareup.picasso.Picasso

class FRMoviePicObject : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fr_movie_pic_object, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.takeIf { it.containsKey(ARG_OBJECT) }?.apply {
            Picasso.get().load(Uri.parse(getString(ARG_OBJECT)))
                .into(view.findViewById(R.id.moviePic) as ImageView)
        }
    }

}
