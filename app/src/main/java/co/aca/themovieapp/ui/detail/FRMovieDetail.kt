package co.aca.themovieapp.ui.detail

import android.os.Bundle
import co.aca.themovieapp.R
import co.aca.themovieapp.base.BaseFragment
import co.aca.themovieapp.repository.response.model.MovieModel
import co.aca.themovieapp.util.extensions.observe
import kotlinx.android.synthetic.main.fr_movie_detail.*
import kotlinx.android.synthetic.main.fr_movie_detail.view.*

class FRMovieDetail : BaseFragment<FRMovieDetailViewModel>() {

    var moviePictureAdapter: MoviePictureAdapter? = null

    companion object {

        fun newInstance(movieModel: MovieModel): FRMovieDetail {

            val data = Bundle()
            data.putSerializable("movieModel", movieModel)

            val fragment = FRMovieDetail()
            fragment.arguments = data

            return fragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fr_movie_detail
    }

    override fun setListeners() {

        imBack.setOnClickListener { activity!!.onBackPressed() }
    }

    override fun setReceivers() {
        observe(viewModel.onMovieDetailReceived, { movieModel: MovieModel? ->

            tvTitle.text = movieModel?.title
            tvPopularity.text = movieModel?.popularity.toString()
            tvReleaseDate.text = movieModel?.release_date
            tvPoint.text = movieModel?.vote_average.toString() + "/10"
            tvOverview.text = movieModel?.overview

            moviePictureAdapter = arrayListOf(
                movieModel?.getPosterFullPath() ?: "",
                movieModel?.getBackdropFullPath() ?: ""
            ).let {
                MoviePictureAdapter(
                    this,
                    it
                )
            }
            viewPager.adapter = moviePictureAdapter
            dotsIndicator.setViewPager2(viewPager)


        })
    }

}
