package co.aca.themovieapp.ui.main

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import co.aca.themovieapp.R
import co.aca.themovieapp.base.BaseActivity
import co.aca.themovieapp.repository.response.model.MovieModel
import co.aca.themovieapp.util.RecyclerScrollListener
import co.aca.themovieapp.util.Util
import co.aca.themovieapp.util.extensions.observe
import kotlinx.android.synthetic.main.ac_movie_list.*

class ACMovieList : BaseActivity<ACMovieListViewModel>(),
    SearchView.OnQueryTextListener {

    private lateinit var gridLayoutManager: GridLayoutManager
    var adapter: MovieListAdapter? = null
    var isListFiltered = false

    override fun getLayoutId(): Int {
        return R.layout.ac_movie_list
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        gridLayoutManager = GridLayoutManager(this, 2)
        super.onCreate(savedInstanceState)

        recyclerView.layoutManager = gridLayoutManager
        setSearchView()
    }

    private fun setSearchView() {

        val searchManager: SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(this)

        val searchViewET = searchView!!.findViewById(R.id.search_src_text) as EditText
        searchViewET.setHintTextColor(resources.getColor(R.color.dark))
        searchViewET.highlightColor = resources.getColor(R.color.black)
        searchViewET.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
    }

    override fun setListeners() {

        swipeRefresh.setOnRefreshListener { viewModel.onSwipeRefreshed() }
        recyclerView.addOnScrollListener(object : RecyclerScrollListener(gridLayoutManager) {
            override fun isLoading(): Boolean {
                return viewModel.isLoading()
            }

            override fun isLastPage(): Boolean {
                return viewModel.isLastPage()
            }

            override fun loadMoreItems() {
                if (!isListFiltered) {
                    viewModel.loadMoreItems()
                }
            }
        })
    }

    override fun setReceivers() {

        observe(viewModel.onShowLoadingListener) {
            swipeRefresh.isRefreshing = true
        }

        observe(viewModel.onHideLoadingListener) {
            swipeRefresh.isRefreshing = false
        }

        viewModel.onMovieListReceived.observe(this, Observer {
            if (it.isNotEmpty()) {

                val index = gridLayoutManager.findFirstVisibleItemPosition()
                val v = recyclerView.getChildAt(0)
                val top = if (v == null) 0 else v.top - recyclerView.paddingTop

                setAdapter(it)

                gridLayoutManager.scrollToPositionWithOffset(index, top)
            }
        })
    }

    private fun setAdapter(list: ArrayList<MovieModel>) {
        adapter = object : MovieListAdapter(this, list) {
            override fun onMovieClicked(movie: MovieModel, position: Int) {
                Util.hideKeyboard(this@ACMovieList)
                viewModel.goToDetailPage(movie)
            }
        }
        recyclerView.adapter = adapter
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        isListFiltered = (newText?.count() ?: 0) > 2
        adapter!!.filter(newText!!)
        return true
    }
}
