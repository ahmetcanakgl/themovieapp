package co.aca.themovieapp.ui.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class MoviePictureAdapter(fragment: Fragment, private val imgList: ArrayList<String>) : FragmentStateAdapter(fragment) {

    companion object {
        const val ARG_OBJECT = "object"
    }

    override fun getItemCount(): Int = imgList.count()

    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        val fragment = FRMoviePicObject()
        fragment.arguments = Bundle().apply {
            putString(ARG_OBJECT, imgList[position])
        }
        return fragment
    }
}