package co.aca.themovieapp.ui.main

import co.aca.themovieapp.repository.response.model.MovieModel
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import co.aca.themovieapp.base.BaseViewModel
import co.aca.themovieapp.domain.network.GetMovieList
import co.aca.themovieapp.repository.base.ErrorModel
import co.aca.themovieapp.repository.response.MovieListResponse
import co.aca.themovieapp.ui.detail.FRMovieDetail
import co.aca.themovieapp.util.extensions.navigateToFragment
import javax.inject.Inject

class ACMovieListViewModel @Inject constructor(val getMovieList: GetMovieList) :
    BaseViewModel(getMovieList) {

    var onMovieListReceived = MutableLiveData<ArrayList<MovieModel>>()
    var onShowLoadingListener = MutableLiveData<Unit>()
    var onHideLoadingListener = MutableLiveData<Unit>()

    override fun onViewCreated(savedInstanceState: Bundle?, arguments: Bundle?) {
        super.onViewCreated(savedInstanceState, arguments)


        getMovieList(false, true)
    }

    fun getMovieList(isSwipeLoadingVisible: Boolean, isGeneralLoadingVisible: Boolean) {

        if (isSwipeLoadingVisible) {
            onShowLoadingListener.postValue(Unit)
        }

        getMovieList.execute(
            GetMovieList.Params(isLoadingVisible = isGeneralLoadingVisible)
        ) { movieListResponse: MovieListResponse?, _: ErrorModel? ->

            if (!movieListResponse?.results.isNullOrEmpty()) {
                onMovieListReceived.postValue(movieListResponse!!.results)
            }

            if (isSwipeLoadingVisible) {
                onHideLoadingListener.postValue(Unit)
            }
        }
    }


    fun loadMoreItems() {
        getMovieList(isSwipeLoadingVisible = false, isGeneralLoadingVisible = false)
    }

    fun isLoading(): Boolean {
        return getMovieList.isLoading
    }

    fun isLastPage(): Boolean {
        return getMovieList.isLastPage
    }

    fun onSwipeRefreshed() {
        onShowLoadingListener.postValue(Unit)
        clearAdapter()
        getMovieList(isSwipeLoadingVisible = true, isGeneralLoadingVisible = false)
    }

    private fun clearAdapter() {
        getMovieList.clearList()
    }

    fun goToDetailPage(movieModel : MovieModel){
        navigateToFragment(FRMovieDetail.newInstance(movieModel))
    }
}