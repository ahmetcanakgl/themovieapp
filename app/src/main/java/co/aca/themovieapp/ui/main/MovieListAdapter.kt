package co.aca.themovieapp.ui.main

import co.aca.themovieapp.repository.response.model.MovieModel
import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.aca.themovieapp.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

abstract class MovieListAdapter(var context: Context, movieList: ArrayList<MovieModel>) :
    RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>() {

    var originalList = ArrayList<MovieModel>()
    var temporaryList = ArrayList<MovieModel>()

    abstract fun onMovieClicked(movie: MovieModel, position: Int)

    init {
        originalList.addAll(movieList)
        temporaryList.addAll(movieList)
    }


    override fun getItemCount(): Int {
        return temporaryList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {

        return MovieViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.movie_item_row,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movieItem = temporaryList[position]

        with(holder) {
            movieTitle.text = movieItem.title

            if (!TextUtils.isEmpty(movieItem.poster_path)) {
                Picasso.get().load(Uri.parse(movieItem.getPosterFullPath()))
                    .into(moviePic)
            }

            itemView.setOnClickListener {
                onMovieClicked(movieItem, position)
            }
        }
    }

    fun filter(filter: String) {
        var filter = filter
        temporaryList.clear()

        if (filter.count() < 3) {
            temporaryList.addAll(originalList)
        } else {
            filter = filter.toLowerCase()
            for (item in originalList) {
                if (item.title!!.toLowerCase(Locale.ROOT).contains(filter) ||
                    item.original_title!!.toLowerCase(Locale.ROOT).contains(filter) ) {
                    temporaryList.add(item)
                }
            }
        }
        notifyDataSetChanged()
    }


    class MovieViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var moviePic: ImageView = itemLayoutView.findViewById(R.id.moviePic)
        var movieTitle: TextView = itemLayoutView.findViewById(R.id.movieTitle)
    }
}
